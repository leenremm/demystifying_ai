import os
os.system("cls")
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.metrics import silhouette_score

# ==============================================================================

print ("Unsupervised clustering method: KMEANS (generated dataset)")

# ==============================================================================

# Creating a sample dataset with clusters
X, y = make_blobs(n_samples=800, n_features=3, centers=4)

# ==============================================================================

# Run kmeans for 2,3,4,5, and 6 clusters, testing with silhouette_score
max_sil_score = 0
for clusters in [2,3,4,5,6]:

    # Initializing KMeans
    kmeans = KMeans(n_clusters=clusters)

    # Fitting with inputs
    kmeans = kmeans.fit(X)

    # Predicting the clusters
    labels = kmeans.predict(X)

    # Getting the cluster centers
    C = kmeans.cluster_centers_
    Y = kmeans.labels_
    error = kmeans.inertia_
    sil_score = silhouette_score(X, Y, metric = 'euclidean')

    # Determine max Score
    if (sil_score > max_sil_score):
        max_sil_score = sil_score
        max_clusters = clusters
        max_C = C
        max_Y = Y

    # Print results:
    print ("KMeans Testing: %d centers. Silhouette Score: %4.3f." % (clusters, sil_score))

# ==============================================================================

# Print results:
print ("\nHighest Silhouette Score:")
print ("KMeans: %d centers. Silhouette Score: %4.3f. \nCluster centers: \n%s\n" % (max_clusters, max_sil_score, max_C))

# Plot
plt.rcParams['figure.figsize'] = (12, 8)
fig = plt.figure()
fig.suptitle('KMeans Clusters: %d. Score: %.3f.' % (max_clusters, max_sil_score), fontsize=20)
ax = Axes3D(fig)
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=max_Y)
ax.scatter(max_C[:, 0], max_C[:, 1], max_C[:, 2], marker='*', c='#050505', s=2000)

plt.show()

print ("End of script")
